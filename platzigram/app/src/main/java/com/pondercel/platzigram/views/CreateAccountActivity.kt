package com.pondercel.platzigram.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.pondercel.platzigram.R

class CreateAccountActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
    }
}
